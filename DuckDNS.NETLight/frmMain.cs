﻿/*This file is part of DuckDNS.NET
    2015 Max J. Rodríguez Beltran maxjrb[at]openitsinaloa.tk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace DuckDNS.NETLight
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private PropertiesJson _propsJson;
        private const string FileName = "prop.json";
        private static readonly string Path = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
        private readonly string _pathString = System.IO.Path.Combine(Path, FileName);
        private int _timerInterval = 120000;

        private string GetExternalIp()
        {
            try
            {
                var externalIp = string.Empty;
                externalIp = (new WebClient()).DownloadString("http://www.showmemyip.com/");
                externalIp = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                    .Matches(externalIp)[0].ToString();
                return externalIp;
            }
            catch (Exception ex)
            {
                LogErrors.LogError("Error: " + ex.Message + " stacktrace: \r\n" + ex.StackTrace);
                return null;
            }
        }

        private void UpdateDomain(string URL)
        {
            try
            {
                using (var wc = new WebClient())
                {
                    wc.DownloadStringCompleted += (sender, e) =>
                    {
                        using (var compWc = (WebClient)sender)
                        {
                            var url = e.UserState as string;
                            Console.WriteLine(compWc.ResponseHeaders[HttpResponseHeader.Server]);
                            Console.WriteLine(url);
                        }
                    };

                    wc.DownloadStringAsync(new Uri(URL));
                }
                lblInfo.Text = "Updated: " + DateTime.Now.ToShortTimeString();
            }
            catch (Exception ex)
            {
                LogErrors.LogError("Error: " + ex.Message + " stacktrace: \r\n"+ ex.StackTrace);
            }

        }

        private void frmMain_Load(object sender, System.EventArgs e)
        {
            lblPublicIP.Text = GetExternalIp();

            timer1.Enabled = true;
            timer1.Start();

            try
            {
                _propsJson =
                    (PropertiesJson)
                        JsonConvert.DeserializeObject(File.ReadAllText(_pathString), (typeof (PropertiesJson)));

                txtDomain.Text = _propsJson.Domain;
                txtToken.Text = _propsJson.Token;
                timer1.Interval = _propsJson.TimerInterval;
                numericUpDown1.Value = _propsJson.TimerInterval/60000;

                var url = string.Format("https://www.duckdns.org/update?domains={0}&token={1}&ip={2}",
                    txtDomain.Text, txtToken.Text, lblPublicIP.Text);

                if (!url.Contains("Empty"))
                    UpdateDomain(url);
            }
            catch (Exception)
            {
                _propsJson = new PropertiesJson();
                txtDomain.Text = _propsJson.Domain;
                txtToken.Text = _propsJson.Token;
                timer1.Interval = _propsJson.TimerInterval;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var url = string.Format("https://www.duckdns.org/update?domains={0}&token={1}&ip={2}",
            txtDomain.Text, txtToken.Text, lblPublicIP.Text);
            
            if (txtDomain.Text.Trim() == String.Empty || txtToken.Text.Trim() == String.Empty)
            {
            }
            else
                UpdateDomain(url);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtToken.Text.Trim() == String.Empty)
                _propsJson.Token = "Empty";

            if (txtDomain.Text.Trim() == String.Empty)
                _propsJson.Domain = "Empty";


            _propsJson.Token = txtToken.Text.Trim();
            _propsJson.Domain = txtDomain.Text.Trim();
            _propsJson.TimerInterval = (int)numericUpDown1.Value * 60000;
            _propsJson.PublicIp = lblPublicIP.Text;

            var jsonp = JsonConvert.SerializeObject(_propsJson);

            try
            {
                File.WriteAllText(_pathString, jsonp);
            }
            catch (Exception ex)
            {
                LogErrors.LogError(ex.Message + ex.StackTrace);
                Console.WriteLine("File Error..., check file permission.");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var url = string.Format("https://www.duckdns.org/update?domains={0}&token={1}&ip={2}",
            txtDomain.Text, txtToken.Text, lblPublicIP.Text);

            if (txtDomain.Text.Trim() == String.Empty || txtToken.Text.Trim() == String.Empty)
            {
            }
            else
                UpdateDomain(url);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox frmAboutBox = new AboutBox();
            frmAboutBox.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    public class PropertiesJson
    {
        public string Token { get; set; }
        public string Domain { get; set; }
        public string PublicIp { get; set; }
        public int TimerInterval { get; set; }

        public PropertiesJson()
        {
            Token = "Your Token Here";
            Domain = "Your Domain Here";
            PublicIp = "xxx.xxx.xxx.xxx";
            TimerInterval = 120000;

        }
    }
}
